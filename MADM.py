"""TOPSIS Implementation"""

#####################################################################
# Federico Cavallo (2016)
# Implementation of TOPSIS, SAW and MEW
#####################################################################

import sys

#for developping, after making setup will be removed
sys.path.append('/home/federico/Dropbox/1_Thesis/12_MADM_Algorithm')
#

import numpy as np
import random as rd

def main():
    """Here an example solving a problem with TOPSIS"""
    
    float_formatter = lambda x: "%.3f" % x                          #Numpy Printing config
    np.set_printoptions(formatter={'float_kind':float_formatter})

    n = 5                                   #n criteria
    m = 8                                   #m options
    np.random.seed()                        #Random example
    W = np.random.rand(n,1)                 #Weight vector
    W = W/np.sum(W)                         #Has to add up to one
    D = np.random.uniform(0,10,(m,n))     #Decision matrix
    Rank_TOPSIS = TOPSIS(n,m,W,D)
    Rank_SAW = SAW(n,m,W,D)
    Rank_MEW = MEW(n,m,W,D)
    print "W:"
    print W
    print "D:"
    print D
    print "TOPSIS Rank:",Rank_TOPSIS
    print "SAW Rank:   ",Rank_SAW
    print "MEW Rank:   ",Rank_MEW

def TOPSIS(n, m, W, D,max_or_min=None,ret = 'rank'):
    """Function return rank by TOPSIS Method. Returns the ranking of positions of best solutions
    counting from 0 to m-1. D: mxn, W: nx1, n criteria, m options"""
    R = np.transpose(np.divide(D,np.sqrt(np.sum(np.power(D,2),0)))) #Normalized Decision Matrix (Euclidean normalization)    
    if abs(np.sum(W) - 1) > 0.001:          #Has to add aproximately one
        raise NameError("W doesn't sum one")
    V = np.multiply(W,R)                    #Weight Normalized decision matrix
    A_pos = np.amax(V,1)                    #Ideal positive and negative solutions
    A_neg = np.amin(V,1)                    #This is made imagining all are desirable criteria
    i = 0
    if max_or_min != None:
        while i<n:
            if not max_or_min[i]: A_pos[i], A_neg[i] = A_neg[i], A_pos[i]
            i += 1
    S_pos = np.sqrt(np.sum(np.power(np.subtract(A_pos, np.transpose(V)),2),1))    #Similarity distance, positive
    S_neg = np.sqrt(np.sum(np.power(np.subtract(A_neg, np.transpose(V)),2),1))    #Similarity distance, negative
    C = S_neg/(S_pos + S_neg)   #Result matrix, alternatives can be ranked in decreising order of C
    Rank = C.argsort()          #Ranking the solutions
    Rank = np.flipud(Rank)
    if ret == 'rank':
        return Rank
    elif ret == 'r':
        return C

def SAW(n, m, W, D):
    """Function returns rank by SAW Method. Returns the ranking of positions of best solutions
    counting from 0 to m-1. D: nxm, W: nx1, n criteria, m options"""
    R = np.transpose(np.divide(D,np.sqrt(np.sum(np.power(D,2),0)))) #Normalized Decision Matrix (Euclidean normalization)    
    if abs(np.sum(W) - 1) > 0.001:          #Has to add aproximately one
        raise NameError("W doesn't sum one")
    V = np.multiply(W,R)        #Weight Normalized decision matrix
    A = np.sum(V,0)             #Sum acros the criterias
    Rank = A.argsort()          #Ranking the solutions
    Rank = np.flipud(Rank)
    
    return Rank

def MEW(n, m, W, D):
    """Function returns rank by MEW Method. Returns the ranking of positions of best solutions
    counting from 0 to m-1. D: nxm, W: nx1, n criteria, m options"""
    #R = np.transpose(np.divide(D,np.sqrt(np.sum(np.power(D,2),0)))) #Normalized Decision Matrix (Euclidean normalization)    
    if abs(np.sum(W) - 1) > 0.001:                  #Has to add aproximately one
        raise NameError("W doesn't sum one")
    V = np.power(np.transpose(D),W)                 #Weight Normalized decision matrix
    A_ideal = np.power(np.transpose(np.max(D,0)),W)
    ro_ideal = np.prod(A_ideal)
    A = np.divide(np.prod(V,0),ro_ideal)            #Prod acros the criterias divided by ideal
    Rank = A.argsort()                              #Ranking the solutions
    Rank = np.flipud(Rank)
    
    return Rank

if __name__ == '__main__':
    sys.exit(main())
